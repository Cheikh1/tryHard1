import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import { products } from '../products';
import {CartService} from "../cart.service";

@Component({
  selector: 'app-products-details',
  templateUrl: './products-details.component.html',
  styleUrls: ['./products-details.component.scss']
})
export class ProductsDetailsComponent implements OnInit {
  addToCart(products) {
    window.alert('Your product has been added to the cart!');
    this.cartService.addToCart(products);
  }
  products;

  constructor(
    private route: ActivatedRoute,
    private cartService: CartService,
  ) { }

  ngOnInit() {

    this.route.paramMap.subscribe(params => {
      this.products = products[+params.get('productId')];
    });
  }

}
