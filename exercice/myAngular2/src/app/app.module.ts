import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from "@angular/common/http";
import {RouterModule} from "@angular/router";
import {ReactiveFormsModule} from "@angular/forms";

import { AppComponent } from './app.component';
import {TopBarComponent} from "./top-bar/top-bar.component";
import {ProductsListComponent} from "./products-list/products-list.component";
import {ProductsAlertsComponent} from "./products-alerts/products-alerts.component";
import { ProductsDetailsComponent } from './products-details/products-details.component';
import { CartComponent } from './cart/cart.component';
import { ShipingComponent } from './shiping/shiping.component';


@NgModule({
  declarations: [
    AppComponent,
    TopBarComponent,
    ProductsListComponent,
    ProductsAlertsComponent,
    ProductsDetailsComponent,
    CartComponent,
    ShipingComponent,

  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    RouterModule.forRoot([
      { path: 'products-list',
        component: ProductsListComponent,
      },
      { path: 'products/:productId',
        component: ProductsDetailsComponent,
      },
      { path: 'cart',
        component: CartComponent,
      },
      { path: 'shipping',
        component: CartComponent,
      },
    ])
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
