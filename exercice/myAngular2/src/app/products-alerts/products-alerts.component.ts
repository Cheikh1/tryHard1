import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-products-alerts',
  templateUrl: './products-alerts.component.html',
  styleUrls: ['./products-alerts.component.scss']
})
export class ProductsAlertsComponent implements OnInit {
  @Input() product;
  @Output() notify = new EventEmitter();


  constructor() { }

  ngOnInit() {
  }

}
