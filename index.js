const express = require('express');
const app = express();

app.use(express.json());
app.use(express.urlencoded({extended : true}));


let hostels = [
    {
        id: 1,
        name: 'hotel rose',
        roomNumbers: 10,
        pool: true,
        rooms: [
            {
                roomName: 'suite de luxe',
                size: 2,
                id: 1
            },
            {
                roomName: 'suite nuptiale',
                size: 2,
                id: 2
            },
            {
                roomName: 'suite familiale',
                size: 4,
                id: 3
            },
            {
                roomName: 'suite budget',
                size: 2,
                id: 4
            },
            {
                roomName: 'suite familiale',
                size: 4,
                id: 5
            },
            {
                roomName: 'suite budget',
                size: 3,
                id: 6
            },
            {
                roomName: 'suite de luxe',
                size: 2,
                id: 7
            },
            {
                roomName: 'suite familiale',
                size: 4,
                id: 8
            },
            {
                roomName: 'suite de luxe',
                size: 3,
                id: 9
            },
            {
                roomName: 'suite présidentielle',
                size: 5,
                id: 10
            }
        ]

    },
    {
        id: 2,
        name: 'hotel ocean',
        roomNumbers: 15,
        pool: false,
        rooms: [
            {
                roomName: 'suite pacifique',
                size: 2,
                id: 1
            },
            {
                roomName: 'suite atlantique',
                size: 2,
                id: 2
            },
            {
                roomName: 'suite manche',
                size: 4,
                id: 3
            },
            {
                roomName: 'suite mer du nord',
                size: 2,
                id: 4
            },
            {
                roomName: 'suite pacifique',
                size: 4,
                id: 5
            },
            {
                roomName: 'suite mer du nord',
                size: 3,
                id: 6
            },
            {
                roomName: 'suite atlantique',
                size: 2,
                id: 7
            },
            {
                roomName: 'suite pacifique',
                size: 4,
                id: 8
            },
            {
                roomName: 'suite atlantique',
                size: 3,
                id: 9
            },
            {
                roomName: 'suite atlantique',
                size: 5,
                id: 10
            },
            {
                roomName: 'suite pacifique',
                size: 2,
                id: 11
            },
            {
                roomName: 'suite mer du nord',
                size: 2,
                id: 12
            },
            {
                roomName: 'suite manche',
                size: 4,
                id: 13
            },
            {
                roomName: 'suite manche',
                size: 3,
                id: 14
            },
            {
                roomName: 'suite mer du nord',
                size: 5,
                id: 15
            },
        ]
    },
    {
        id: 3,
        name: 'hotel des Pins',
        roomNumbers: 7,
        pool: true,
        rooms: [
            {
                roomName: 'suite bordelaise',
                size: 2,
                id: 1
            },
            {
                roomName: 'suite marseillaise',
                size: 2,
                id: 2
            },
            {
                roomName: 'suite nicoise',
                size: 4,
                id: 3
            },
            {
                roomName: 'suite canoise',
                size: 2,
                id: 4
            },
            {
                roomName: 'suite hendaiar',
                size: 4,
                id: 5
            },
            {
                roomName: 'suite canoise',
                size: 3,
                id: 6
            },
            {
                roomName: 'suite nicoise',
                size: 2,
                id: 7
            }
        ]
    }
];



// exercice A

// faire une fonction qui ajoute un nouvel hotel dans la liste (fabriquer vous même le nouvel hotel).
// Le nom de la fonction doit etre addHostel

const newHostel = {

    name: 'white rabbit',
    roomNumbers: 7,
    pool: true,
    rooms: [
        {
            roomName: 'landing',
            size: 2,
            id: 1
        },
        {
            roomName: 'ben said yassine',
            size: 2,
            id: 2
        },
        {
            roomName: 'khadija sarr',
            size: 4,
            id: 3
        },
        {
            roomName: 'abdel ait bellaid',
            size: 2,
            id: 4
        },
        {
            roomName: 'bouazizi jihen',
            size: 4,
            id: 5
        },
        {
            roomName: 'chouk sokvadara',
            size: 3,
            id: 6
        },
        {
            roomName: 'jonathan valaydoum',
            size: 2,
            id: 7
        }
    ]
};

//console.log('exercice A');

function addHostel(hostel) {
    const buffer = {...hostel};
    buffer.id = hostels.length + 1;
     hostels.push(buffer);

     return buffer

}

addHostel(newHostel);

//console.log(hostels);



// exercice B
// faire une fonction removeHostel qui enleve un hotel en prenant en paramètre l'id de l'hotel


//console.log('exercice B');

function removeHostel(hostelId) {
   const result =  hostels.filter(hostel => hostel.id !== hostelId);

    if (result) {
        return result
    } else {
        return  'pas dhostel avec cette hostelId'
    }

}
//removeHostel(2);
//console.log(hostels);


// exercice c
// faire une fonction qui remplace un hotel  partir de son id par un nouvel hostel putHostel

function putHostel(id, newHostel) {
    const index = hostels.findIndex(element => element.id === id);
    newHostel.id = id;
    hostels[index] = newHostel;
    return newHostel;
}
//putHostel(1, newHostel);

//console.log(hostels);

// exercice d
// faire une fonction qui renvoie un hotel à partir de son id

function getHostelById(hostelId){

   const result = hostels.find(element => element.id === hostelId);

   if (result) {
       return result
   } else {
       return  'pas dhostel avec cette hostelId'
   }

}
//getHostelById(2);
//console.log(hostels);



app.get('/', function (req, res) {
    res.send('Hello World!')
});


app.get('/hostels/:hostelId', function (req, res) {

    const hostelId = parseInt(req.params.hostelId);

console.log(hostelId);
    res.send(getHostelById(hostelId));
});



app.delete('/hostels/:hostelId', function (req, res) {

    const hostelId = parseInt(req.params.hostelId);

    console.log(hostelId);
    res.send(removeHostel(hostelId));
});


app.post('/hostels', function (req, res){

    const hostelToCreate = req.body;

    const createdHostel = addHostel(hostelToCreate);

    res.status(201).send(createdHostel);

});


app.put('/hostels/:id', function (req, res){

    const hostelTocreate = req.body;

    const id = parseInt(req.params.id);

    const newHostel = putHostel(id, hostelTocreate);

    res.send(newHostel);

});


app.patch('/hostels/:id', function (req, res){

   try {
       const dataToChange = req.body;

       const keysTochange = Object.keys(dataToChange);

       const id = parseInt(req.params.id);

       const index = hostels.findIndex(element => element.id === id);


       let hostelToChange = hostels[index];

       const authorizedKeys = Object.keys(hostelToChange);

       const isAuthorized =

           keysTochange.map(key => authorizedKeys.includes(key));

       const isOk = isAuthorized.every(key => key);

       if (!isOk){

           throw new Error('non authorized key');

       }

       Object.assign(hostelToChange, dataToChange);

       //putHostel(id, hostelToChange);



       console.log(hostelToChange);
       console.log(hostels);
       res.send(hostelToChange);


   } catch (e) {
       console.log(e.message);
       res.status(400).send(e.message);
   }

});



app.listen(3000, function () {
    console.log('Example app listening on port 3000!')
});



// 