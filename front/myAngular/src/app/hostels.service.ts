import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {HostelModel} from "../../../../tsSrc/functions/src/models/hostel.model"

import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class HostelsService {
  private HttpClient: HttpClient;

  constructor(
    HttpClient: HttpClient
  ) {
    this.HttpClient = HttpClient;
  }

gethostels$(): Observable<HostelModel[]> {
    return this.HttpClient.get<HostelModel[]>('http://localhost:5000/melzi-6f2a5/us-central1/helloWorld')

  }



}
