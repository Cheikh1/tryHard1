import { Component, OnInit } from '@angular/core';
import {PoneysService} from "../../services/poneys.service";
import {tap} from "rxjs/operators";
import {PoneyModel} from "../../models/poneys.model";

@Component({
  selector: 'app-poneys-list',
  templateUrl: './poneys-list.component.html',
  styleUrls: ['./poneys-list.component.scss']
})
export class PoneysListComponent implements OnInit {

  poneys: PoneyModel[];

  constructor(
    private poneysService: PoneysService,
  ) { }

  ngOnInit() {
    this.poneysService.getAllPoneys()
      .pipe(
        tap((poneys: PoneyModel[]) => this.poneys = poneys)
      )
      .subscribe()
  }

}
