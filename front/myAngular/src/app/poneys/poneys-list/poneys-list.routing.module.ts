import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PoneysListComponent} from "./poneys-list.component";


const routes: Routes = [
  {
    path: '',
    component: PoneysListComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PoneysListRoutingModule { }
