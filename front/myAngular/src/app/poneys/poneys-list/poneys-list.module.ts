import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PoneysListComponent } from './poneys-list.component';
import {PoneysListRoutingModule} from "./poneys-list.routing.module";



@NgModule({
  declarations: [PoneysListComponent],
  imports: [
    CommonModule,
    PoneysListRoutingModule,
]
})
export class PoneysListModule { }
