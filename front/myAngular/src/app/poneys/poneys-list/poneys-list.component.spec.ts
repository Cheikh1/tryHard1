import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PoneysListComponent } from './poneys-list.component';

describe('PoneysListComponent', () => {
  let component: PoneysListComponent;
  let fixture: ComponentFixture<PoneysListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PoneysListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PoneysListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
