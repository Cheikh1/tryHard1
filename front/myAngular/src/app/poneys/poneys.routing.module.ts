import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PoneysComponent} from "./poneys.component";


const routes: Routes = [
  {
    path: '',
    component: PoneysComponent,
    children: [
      {
        path: 'poneys-list',
        loadChildren: () => import('./poneys-list/poneys-list.module').then(m => m.PoneysListModule)
      },
      {
        path: ':id',
        loadChildren: () => import('./poneys-detail/poneys-detail.module').then(m => m.PoneysDetailModule)
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PoneysRoutingModule { }
