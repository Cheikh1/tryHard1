import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PoneysComponent } from './poneys.component';

describe('PoneysComponent', () => {
  let component: PoneysComponent;
  let fixture: ComponentFixture<PoneysComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PoneysComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PoneysComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
