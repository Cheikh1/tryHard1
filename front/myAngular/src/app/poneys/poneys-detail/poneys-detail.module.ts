import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PoneysDetailComponent } from './poneys-detail.component';
import {PoneysDetailRoutingModule} from "./poneys-detail.routing.module";
import {ReactiveFormsModule} from "@angular/forms";



@NgModule({
  declarations: [PoneysDetailComponent],
  imports: [
    CommonModule,
    PoneysDetailRoutingModule,
    ReactiveFormsModule,
  ]
})
export class PoneysDetailModule { }
