import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PoneysDetailComponent } from './poneys-detail.component';

describe('PoneysDetailComponent', () => {
  let component: PoneysDetailComponent;
  let fixture: ComponentFixture<PoneysDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PoneysDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PoneysDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
