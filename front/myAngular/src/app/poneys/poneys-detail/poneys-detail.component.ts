import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {PoneysService} from "../../services/poneys.service";

@Component({
  selector: 'app-poneys-detail',
  templateUrl: './poneys-detail.component.html',
  styleUrls: ['./poneys-detail.component.scss']
})
export class PoneysDetailComponent implements OnInit {
  poneyForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private poneysService: PoneysService,
  ) { }

  ngOnInit() {
    this.poneyForm = this.fb.group({
      name : ['toto', [Validators.required, Validators.minLength(3), Validators.maxLength(20)]],
      age : [null, [Validators.required]],
      color : [null, [Validators.required]],
      size : [null, []],
    })
  }
  onSubmit() {
    console.log(this.poneyForm.value);
    this.poneysService.addNewPoney$(this.poneyForm.value).subscribe()
  }

}
