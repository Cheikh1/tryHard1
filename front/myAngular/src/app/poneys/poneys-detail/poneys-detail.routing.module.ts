import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PoneysDetailComponent} from "./poneys-detail.component";


const routes: Routes = [
  {
    path: '',
    component: PoneysDetailComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PoneysDetailRoutingModule { }
