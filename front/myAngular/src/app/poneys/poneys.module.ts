import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PoneysComponent } from './poneys.component';
import {PoneysRoutingModule} from "./poneys.routing.module";



@NgModule({
  declarations: [PoneysComponent],
  imports: [
    CommonModule,
    PoneysRoutingModule,
  ]
})
export class PoneysModule { }
