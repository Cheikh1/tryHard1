export interface PoneyModel {
  name : string;
  age : number;
  color : string;
  size : number;
}
