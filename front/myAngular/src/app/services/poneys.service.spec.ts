import { TestBed } from '@angular/core/testing';

import { PoneysService } from './poneys.service';

describe('PoneysService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PoneysService = TestBed.get(PoneysService);
    expect(service).toBeTruthy();
  });
});
