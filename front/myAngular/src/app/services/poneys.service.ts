import { Injectable } from '@angular/core';

import {PoneyModel} from "../models/poneys.model";
import {from, Observable} from "rxjs";
import {AngularFirestore, DocumentReference} from "@angular/fire/firestore";

@Injectable({
  providedIn: 'root'
})
export class PoneysService {

  constructor(
    private afs: AngularFirestore
  ) { }
  addNewPoney$(poney: PoneyModel): Observable<DocumentReference>{
    return from(this.afs.collection('poneys').add(poney));
  }

  getAllPoneys(): Observable<PoneyModel[]>{
    return this.afs.collection<PoneyModel>('poneys').valueChanges()
  }
}
