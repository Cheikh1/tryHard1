import {Component, OnInit} from '@angular/core';
import {HostelsService} from "./hostels.service";
import {HostelModel} from "../../../../tsSrc/functions/src/models/hostel.model";
import {tap} from "rxjs/operators";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  hostels: HostelModel[];
constructor(
  private HostelsService: HostelsService,
) {

}

getHostels() {
  this.HostelsService.gethostels$()
    .pipe(
      tap((hostels:HostelModel[]) => this.hostels = hostels),
      tap((hostels:HostelModel[]) => console.log(this.hostels = hostels)),

    )
    .subscribe()
}
ngOnInit(): void {
  this.getHostels();
}
}
export class HeaderComponent implements OnInit{
  ngOnInit(): void {
  }
}
