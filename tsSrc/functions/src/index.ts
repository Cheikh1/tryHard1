import *as functions from 'firebase-functions';


import *as express from "express";
import {addHostel, patchHostel, putHostel, removeHostel} from "./utilities";
import * as admin from "firebase-admin";
import {RoomClass} from "./models/room.model";
import {HostelClass} from "./models/hostel.model";
import {CityClass} from "./models/city.model";


const cors = require('cors');

const app = express();
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

const db = admin.firestore();


app.get('/', function (req, res) {

    try {


        /*const hostels: HostelModel[] = await getHostels();
        const hostelClass = hostels.map((hostel: HostelModel) => new HostelClass(hostel));
        res.send(hostelClass);*/

        const room1: RoomClass = new RoomClass({
            roomName: 'suite de luxe',
            size: 2,
            id: 1
        });

        const room2: RoomClass = new RoomClass({
            roomName: 'suite de luxe',
            size: 4,
            id: 2
        });

        const room3: RoomClass = new RoomClass({
            roomName: 'suite de luxe',
            size: 2,
            id: 3
        });


        const room4: RoomClass = new RoomClass({
            roomName: 'suite de LUXEEEEEEE',
            size: 4,
            id: 4
        });

        const hostel1 = new HostelClass({
            id: 1,
            name: 'hotel des Pins',
            pool: true,
            rooms: [room1, room2, room3, room4]
        });


        const hostel2 = new HostelClass({
            id: 2,
            name: 'hotel white rabbit',

            pool: true,
            rooms: [room1, room2, room3]
        });


        const city = new CityClass([hostel1, hostel2]);

        const allRooms = city
            .getAllRooms();
        const filteredRooms = CityClass.filterRooms(allRooms);
        const sortedRooms = CityClass.sortedRooms(filteredRooms);



        res.send(sortedRooms);

    } catch (e) {
        res.status(500).send(e)
    }
    /*const myHostel = new LuxHostelClass({
        id: 12,
        name: 'hotel white rabbit',
        rooms: [],
        numberOfRooftops: 2,
        numberOfStars: 4,
    });
    myHostel
        .calculateRoomNumbers()
        .createPool();
    res.send(myHostel);*/


    /* const myRoom = new RoomClass(12);
     res.send(myRoom);*/

});


app.get('/', async function (req, res) {
    try {
        const hostelPromise = db
            .collection('hostels')
            .doc('6FWrV1jbqbXrleScKKfA')
            .get();

        const hostel2promise = db
            .collection('hostels')
            .doc('8R4hTZtR2lOpMNzFt0YF')
            .get();

        const [hostelSnapshot, hostel2Snapshot] = await Promise.all([
            hostelPromise,
            hostel2promise
        ]);

        const hostel = hostelSnapshot.data();
        const hostel2 = hostel2Snapshot.data();
        res.send([hostel, hostel2]);

    } catch (e) {
        console.log(e);

    }

});

/*app.get('/', function (req, res){
    const promiseArray = [];
    let i = 0;
    for(; i < 10; i++) {
        promiseArray.push(db.collection('test').doc(i + '').delete())
    }

    Promise.all(promiseArray)
        .then(() => res.send('document deleted: ' + i + ''))
        .catch(e => console.log(e))

});*/

function randomize(start: number, end: number) {
    return Math.floor(Math.random() * end + start);
}

app.get('/:id', function (req, res) {

    const firstNames = ['chouk', 'khadija', 'john', 'melzi', 'kamel', 'walid', 'takahiro', 'yassine', 'llyes'];
    const lastNames = ['sokvadara', 'sarr', 'velaydoum', 'panyagua', 'kissi', 'oualdito', 'taka', 'ait ahmed', 'nekkaz'];


    const users = [];
    const usersId = parseInt(req.params.id);
    let i = 0;
    for (; i < usersId; i++) users.push(
        db
            .collection('users')
            .doc(i + '')
            .set({
                firstName: firstNames[randomize(0, lastNames.length)],
                lastName: lastNames[randomize(0, lastNames.length)],
                age: randomize(0, 99)
            }));

    Promise.all(users)
        .then(() => res.send('document writed: ' + i + ''))
        .catch(e => console.log(e))


});


app.delete('/:id', function (req, res) {

    const users = [];
    const usersId = parseInt(req.params.id);
    let i = 0;
    for (; i < usersId; i++) users.push(
        db
            .collection('users')
            .doc(i + '')
            .delete());

    Promise.all(users)
        .then(() => res.send('document deleted: ' + i + ''))
        .catch(e => console.log(e))


    /*app.get('/hostels/:hostelId', function (req, res) {
        const hostelId = req.params.hostelId;
        getHostelById(hostelId)
            .then(hostel => res.send(hostel.data()))
            .catch(e => res.send(e))
        ;
    });*/

    /*app.get('/hostels/:hostelId', async function (req, res) {
        try {
            const hostelId = req.params.hostelId;

            const newHostel = await getHostelById(hostelId);

            res.send(newHostel.data());

        } catch (e) {
            console.log(e);
            res.status(500).send(e)

        }*/

});


app.delete('/hostels/:hostelId', function (req, res) {
    const hostelId = req.params.hostelId;

    removeHostel(hostelId)
        .then(result => res.send(result))
        .catch(e => res.status(500).send(e))
    ;
});


app.delete('/hostels/:hostelId', async function (req, res) {
    try {
        const hostelId = req.params.hostelId;

        res.send(await removeHostel(hostelId));


    } catch (e) {
        console.log(e);
        res.status(500).send(e)

    }

});


app.post('/hostels', function (req, res) {
    const hostelToCreate = req.body;
    addHostel(hostelToCreate)
        .then(document => document.get())
        .then(createdHostel => res.status(201).send(createdHostel.data()))
        .catch(e => res.status(500).send(e))
    ;
});


app.post('/hostels', async function (req, res) {
    try {
        const hostelToCreate = req.body;

        res.send(await addHostel(hostelToCreate));

    } catch (e) {
        console.log(e);
        res.status(500).send(e)

    }

});


app.put('/hostels/:id', function (req, res) {
    const hostelTocreate = req.body;
    const id = req.params.id;
    putHostel(id, hostelTocreate)
        .then(hostelcreated => res.send(hostelcreated))
        .catch(e => res.status(500).send(e))
    ;
});


app.put('/hostels/:id', async function (req, res) {
    try {
        const hostelToCreate = req.body;
        const id = req.params.id;
        res.send(await putHostel(id, hostelToCreate));

    } catch (e) {
        console.log(e);
        res.status(500).send(e)

    }

});


app.patch('/hostels/:id', function (req, res) {
    const hostelTocreate = req.body;
    const id = req.params.id;
    patchHostel(id, hostelTocreate)
        .then(hostelcreated => res.send(hostelcreated))
        .catch(e => res.status(500).send(e))
    ;
});


app.put('/hostels/:id', async function (req, res) {
    try {
        const hostelToCreate = req.body;
        const id = req.params.id;
        res.send(await patchHostel(id, hostelToCreate));

    } catch (e) {
        console.log(e);
        res.status(500).send(e)

    }

});


export const helloWorld = functions.https.onRequest(app);


//les boucles for

//random = aléatoire
// boucle for est u outil pour faire une boucle. elle a deux premieres parties : conditionnel/executer => foction de callback

//i++ => i = i + 1