import {HostelModel} from "./hostel.model";

//import {RoomClass} from "./room.model";

export interface CityModel {
    hostels: HostelModel[];
}

export class CityClass implements CityModel {
    hostels: HostelModel[];

    constructor(hostels: HostelModel[]) {
        this.hostels = hostels;
        console.log(this.hostels[0])

    }

    orderHostelByRoomsSize() {
        this.hostels.sort((a, b) => (a.roomNumbers as number) - (b.roomNumbers as number));
        return this;
    }

    getArrayWithHostelsName() {
        return this.hostels.map((hostel) => hostel.name);

    }

    getAllRooms() {

        return this.hostels.reduce((previousValue: any, currentValue: any) => {


            return previousValue.concat(currentValue.rooms)
        }, []);

    }

     static filterRooms(rooms:any)  {

             return rooms.filter((room:any) => room.size >= 3);

     }


    static sortedRooms(rooms: any) {
        return rooms.sort((a: any, b: any) => a.roomName < b.roomName ? -1:1);
    }









    /*
        sortFunc() {
            this.rooms.sort((a: { roomName: string; }, b: { roomName: string; }) => ((a.roomName as string) < (b.roomName as string) ? -1 : 1));
        }
    */


}








