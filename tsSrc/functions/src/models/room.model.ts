//import {object} from "firebase-functions/lib/providers/storage";

export interface RoomModel {
    roomName?: string;
    size?: number;
    id?: number;
}

export class RoomClass implements RoomModel {
    roomName?: string;
    size?: number;
    id?: number;

    constructor(room:RoomModel) {
       this.roomName = room.roomName;
       this.id = room.id;
       this.size = room.size;
    }
}













//ANGULAR => framework JS (modules/compenents)