import {RoomModel} from "./room.model";
//import {hostels} from "../database.data";

export interface HostelModel {
    id?: number;
    name?: string;
    roomNumbers?: number;
    pool?: boolean;
    rooms: RoomModel[];
    isValidated?: boolean;
}

export class HostelClass {
    id?: number;
    name?: string;
    roomNumbers?: number;
    pool?: boolean;
    isValidated?: boolean;
    rooms: RoomModel[];


   constructor (hostel: HostelModel) {
       this.rooms = hostel.rooms;
       Object.assign(this, hostel);
       this.calculateRoomNumbers()

   }


calculateRoomNumbers(): HostelClass {
       this.roomNumbers = this.rooms.length;
       return this;
   }

    calculateRoomNumbers2() {
        this.roomNumbers = this.rooms.length;
        return this.roomNumbers;
    }

   destroyPool(): HostelClass {
       this.pool = false;
       return this;
   }

    createPool(): HostelClass {
        this.pool = true;
        return this;
    }
}


export interface LuxHostelModel extends HostelModel{
    numberOfStars: number;
    numberOfRooftops: number;
}

export class LuxHostelClass extends HostelClass{

    numberOfStars: number;
    numberOfRooftops: number;

    constructor (hostel: LuxHostelModel) {
        super(hostel);
        this.numberOfStars = hostel.numberOfStars || 1;
        this.numberOfRooftops = hostel.numberOfRooftops || 1;

    }


}




