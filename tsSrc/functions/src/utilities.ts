import {HostelModel} from "./models/hostel.model";
import *as admin from 'firebase-admin';
import { QuerySnapshot } from "@google-cloud/firestore";
let serviceAccount = require('./key.json');
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount)
});


const db = admin.firestore();


export function addHostel(hostel: HostelModel): Promise<FirebaseFirestore.DocumentReference> {
    return db
        .collection('hostels')
        .add(hostel)
}


export function removeHostel(hostelId: string): Promise<FirebaseFirestore.WriteResult> {
    return db
        .collection('hostels')
        .doc(hostelId)
        .delete()

}


export function putHostel(id: string, newHostel: HostelModel) {
    return db
        .collection('hostels')
        .doc(id)
        .set(newHostel)

}


export function patchHostel(id: string, newHostel: HostelModel) {
    return db
        .collection('hostels')
        .doc(id)
        .update(newHostel)

}



export async function getHostels() {
    const list: QuerySnapshot = await db
        .collection('hostels').get();
    const hostels: HostelModel[] = [];
    list.forEach(doc => hostels.push(doc.data() as HostelModel));
    return hostels;


}

//const result: number = hostels.findIndex(element => element.id === hostelId);
//return hostels[result];