
let hostels = [
    {
        id: 1,
        name: 'hotel rose',
        roomNumbers: 10,
        pool: true,
        rooms: [
            {
                roomName: 'suite de luxe',
                size: 2,
                id: 1
            },
            {
                roomName: 'suite nuptiale',
                size: 2,
                id: 2
            },
            {
                roomName: 'suite familiale',
                size: 4,
                id: 3
            },
            {
                roomName: 'suite budget',
                size: 2,
                id: 4
            },
            {
                roomName: 'suite familiale',
                size: 4,
                id: 5
            },
            {
                roomName: 'suite budget',
                size: 3,
                id: 6
            },
            {
                roomName: 'suite de luxe',
                size: 2,
                id: 7
            },
            {
                roomName: 'suite familiale',
                size: 4,
                id: 8
            },
            {
                roomName: 'suite de luxe',
                size: 3,
                id: 9
            },
            {
                roomName: 'suite présidentielle',
                size: 5,
                id: 10
            }
        ]

    },
    {
        id: 2,
        name: 'hotel ocean',
        roomNumbers: 15,
        pool: false,
        rooms: [
            {
                roomName: 'suite pacifique',
                size: 2,
                id: 1
            },
            {
                roomName: 'suite atlantique',
                size: 2,
                id: 2
            },
            {
                roomName: 'suite manche',
                size: 4,
                id: 3
            },
            {
                roomName: 'suite mer du nord',
                size: 2,
                id: 4
            },
            {
                roomName: 'suite pacifique',
                size: 4,
                id: 5
            },
            {
                roomName: 'suite mer du nord',
                size: 3,
                id: 6
            },
            {
                roomName: 'suite atlantique',
                size: 2,
                id: 7
            },
            {
                roomName: 'suite pacifique',
                size: 4,
                id: 8
            },
            {
                roomName: 'suite atlantique',
                size: 3,
                id: 9
            },
            {
                roomName: 'suite atlantique',
                size: 5,
                id: 10
            },
            {
                roomName: 'suite pacifique',
                size: 2,
                id: 11
            },
            {
                roomName: 'suite mer du nord',
                size: 2,
                id: 12
            },
            {
                roomName: 'suite manche',
                size: 4,
                id: 13
            },
            {
                roomName: 'suite manche',
                size: 3,
                id: 14
            },
            {
                roomName: 'suite mer du nord',
                size: 5,
                id: 15
            },
        ]
    },
    {
        id: 3,
        name: 'hotel des Pins',
        roomNumbers: 7,
        pool: true,
        rooms: [
            {
                roomName: 'suite bordelaise',
                size: 2,
                id: 1
            },
            {
                roomName: 'suite marseillaise',
                size: 2,
                id: 2
            },
            {
                roomName: 'suite nicoise',
                size: 4,
                id: 3
            },
            {
                roomName: 'suite canoise',
                size: 2,
                id: 4
            },
            {
                roomName: 'suite hendaiar',
                size: 4,
                id: 5
            },
            {
                roomName: 'suite canoise',
                size: 3,
                id: 6
            },
            {
                roomName: 'suite nicoise',
                size: 2,
                id: 7
            }
        ]
    }
];


// exercice A

// faire une fonction qui ajoute un nouvel hotel dans la liste (fabriquer vous même le nouvel hotel).
// Le nom de la fonction doit etre addHostel

const newHostel = {

    name: 'white rabbit',
    roomNumbers: 7,
    pool: true,
    rooms: [
        {
            roomName: 'landing',
            size: 2,
            id: 1
        },
        {
            roomName: 'ben said yassine',
            size: 2,
            id: 2
        },
        {
            roomName: 'khadija sarr',
            size: 4,
            id: 3
        },
        {
            roomName: 'abdel ait bellaid',
            size: 2,
            id: 4
        },
        {
            roomName: 'bouazizi jihen',
            size: 4,
            id: 5
        },
        {
            roomName: 'chouk spkvadara',
            size: 3,
            id: 6
        },
        {
            roomName: 'jonathan valaydoum',
            size: 2,
            id: 7
        }
    ]
};
console.log('exercice A');

function addHostel(hostel) {
    const buffer = {...hostel};
    buffer.id = hostels.length + 1;
    return hostels.push(buffer);

}

addHostel(newHostel);

console.log(hostels);



// exercice B
// faire une fonction removeHostel qui enleve un hotel en prenant en paramètre l'id de l'hotel


console.log('exercice B');

function removeHostel(hostelId) {
   hostels =  hostels.filter(hostel => hostel.id !== hostelId);

    return hostels

}
removeHostel(2);
console.log(hostels);


// exercice c
// faire une fonction qui remplace un hotel  partir de son id par un nouvel hostel putHostel

function putHostel(id, y) {
    const index = hostels.find(element => element.id === id);
    hostels[index] = y;
    y.id = id;
    return hostels;
}
putHostel(1, newHostel);

console.log(hostels);

// exercice d
// faire une fonction qui renvoie un hotel à partir de son id

function getHostelById(hostelId){

    return hostels.find(element => element.id === hostelId);

}
getHostelById(2);
console.log(hostels);

// exercice e
// faire la meme chose avec les rooms des hostels

const newRoom = {
    roomName: 'jupiter',
    size: 2,
};


//exercice e.a

function addRoom(hostelId, roomId) {

    const hostel = getHostelById(hostelId);
    const buffer = {...roomId};

    buffer.id = hostel.rooms.length + 1;

    hostel.rooms.push(buffer);

    hostel.roomNumbers = hostel.rooms.length;

    return hostels

}
addRoom(1, newRoom);
console.log(hostels);

// exercice e.b


function removeRoom(hostelId, roomId) {


    const room = getHostelById(hostelId);

    room.rooms = room.rooms.filter(room => room.id !== roomId);

    room.roomNumbers = room.rooms.length;

    return hostels

}

removeRoom(2, 1);

console.log(hostels);


//exercice e.c


function putRoom(hostelId,roomID, room){

    const put = getHostelById(hostelId);

    const index = put.rooms.findIndex(value => value.id === roomID);
    room.id = roomID;
    put.rooms[index] = room;

    return hostels
}
putRoom(1, 3, newRoom);
console.log(putRoom(1, 2, newRoom));


//exercice e.d

function getRoomById(hostelId, roomId){

    const getRoom = getHostelById(hostelId);

    const newIndex = getRoom.rooms.findIndex(element => element.id === roomId);

    return getRoom.rooms[newIndex];

}
getRoomById(1, newRoom);

console.log(getRoomById(1, 2));


// exercice 1 : trier les hotels par nombre de chambres (plus grand en 1er) et créer un tableau contenant seulement
// le nom des hotels dans leur ordre de tri

console.log('exercice 1');
console.log(hostels.sort((a, b) => b.roomNumbers - a.roomNumbers).map(value => value.name));



// exercice 2 : faire un tableau avec toutes les chambres de tous les hotels, et ne garder que les chambres qui
// ont plus que 3 places ou exactement 3 places et les classer par ordre alphabétique selon le non de la chambre


//console.log(hostels.map(hotel => { hotel.rooms = hotel.rooms.filter(room => room.size >= 3).sort((a, b) => (a.roomName < b.roomName) ? -1 : (a.roomName > b.roomName) ? 1 : 0); return hotel;}));



const tab1 = hostels.reduce((previousValue, currentValue) => {

   return previousValue.concat(currentValue.rooms).filter(room => room.size >= 3).sort((a, b) => a.roomName < b.roomName ? -1:1);

}, []);


console.log('exercice 2',tab1);




// exercice 3 : mettre une majuscule à tous les mots qui sont dans l'attribut RoomName


//console.log('exercice 3');


hostels = hostels.map(hostel => { hostel.rooms.forEach(room => room.roomName = room.roomName
    .split(' ')
    .map(word => word.charAt(0).toUpperCase() + word.slice(1))
    .join(' '));
return hostel
});

console.log(hostels);



// exercice 4 : enlever toutes les chambres qui ont plus de 3 places et changer la valeur de roomNumbers pour qu'elle reflete
// le nouveau nombre de chambres

console.log('exercice 4');

hostels = hostels
    .map(hostel => { hostel.rooms = hostel.rooms
    .filter(hostel => hostel.size < 4)
    hostel.roomNumbers = hostel.rooms.length;


    return hostel

});

console.log(hostels);


// exercice 5  : extraire du tableau hostels l'hotel qui a le nom 'hotel ocean' en le supprimant du tableau, et le mettre dans une nouvelle variable
// puis effacer toutes ses chambres et mettre à jour sa valeur room number, puis pusher l'hotel modifié dans hostels, puis faire un sort par nom d'hotel
// puis donner le nouvel index de l'hotel océan (faire 2 méthodes : avec indexOf et avec un foreach)


const ocean = hostels.findIndex(element => element.name === 'hotel ocean' );
const [hotelOcean] = hostels.splice(ocean, 1);
      [hotelOcean].map(element => (element.rooms = []));
      [hotelOcean].map(element1 => element1.roomNumbers = element1.rooms.length);
      hostels.push(hotelOcean);
      hostels.sort((a, b) => (a.name < b.name) ? -1:1);

const newIndex = hostels.indexOf(hotelOcean);

console.log(ocean);
console.log([hostels]);
console.log(newIndex);



// exercice 6 : créer un objet dont les clés sont le nom des hotels et dont la valeur est un booléen qui indique si l'hotel a une chambre qui s'appelle 'suite marseillaise'

const tab = {...hostels.map(value => value.name
        +' : '
        +value.rooms.some(value => value.roomName === 'suite marseillaise'))};

console.log(tab);


// liste des falsy : false, 0, nul, '', NaN, undefined,
