// exercice 1 : générer un array avec 100 objets avec la forme ci dessous, dont les données sont toutes aléatoires
//const person = {firstName: '', lastName: '', age: 0};
// bonus : calculer la moyenne des âges de personnes en utilisant un reduce

function randomize(start, end) {
    return Math.floor(Math.random() * end + start);
}

const person = [];
const firstNames = ['chouk', 'khadija', 'john', 'melzi', 'kamel', 'walid', 'takahiro', 'yassine', 'llyes'];
const lastNames = ['sokvadara', 'sarr', 'velaydoum', 'panyagua', 'kissi', 'oualdito', 'taka', 'ait ahmed', 'nekkaz'];


for (let i = 0; i < 100; i++) {
    person.push({
        firstName: firstNames[randomize(0, lastNames.length)],
        lastName: lastNames[randomize(0, lastNames.length)],
        age: randomize(0, 99)
    });
    Math.floor(person.reduce((previousValue, currentValue) => previousValue + currentValue.age / 100, 0))
}
console.log('EXERCICE 1', person);

// exercice 2 : générer un tableau contenant des nombres pairs consécutifs, le premier nombre du tableau doit être 4,
// on doit arreter de remplir le tableau quand il y a 20 nombres pairs dans le tableau

const tab = [];

for (let i = 4; tab.length < 20; i += 2) {
    tab.push(i);
}
console.log('EXERCICE 2', tab);
// exercice 3 : Écrire un programme qui affiche les nombres de 1 à 199 avec un console log.
// Mais pour les multiples de 3, afficher “Fizz” au lieu du nombre et pour les multiples de 5 afficher “Buzz”.
// Pour les nombres multiples de 3 et 5, afficher “FizzBuzz”.
const melzi = [];
for (let i = 1; i <= 200; i++) {
    if (i % 3 === 0 && i % 5 === 0) {
        melzi.push("Fizzbuzz")
    } else if (i % 3 === 0) {
        melzi.push("Fizz");
    } else if (i % 5 === 0) {
        melzi.push("BUZZ")
    } else {
      melzi.push(i)
    }
}
console.log('Exercice 3', melzi);
// leçon du mardi 5 novembre (comment créer des promesses pour les rentrer d'embauche)

//a wait a think, il faut systematiquent commencer par try catch